/**
 * Created with a5.
 * User: nicksegal
 * Date: 2014-11-18
 * Time: 04:21 AM
 * To change this template use Tools | Templates.
 */



function register() {
    //var fn = document.getElementById('#user_first_name');
    //var ln = document.getElementById('#user_last_name');
    //var un = document.getElementById('#user_username');
    //var pw = document.getElementById('#user_password');
    
    var fn = $('#user_first_name').val();
    var ln = $('#user_last_name').val();
    var un = $('#user_username').val();
    var pw = $('#user_password').val();
    
    localStorage.setItem("vmworld.firstname", JSON.stringify(fn));
    localStorage.setItem("vmworld.lastname", JSON.stringify(ln));
    localStorage.setItem("vmworld.username", JSON.stringify(un));
    localStorage.setItem("vmworld.password", JSON.stringify(pw));
    
    localStorage.setItem("vmworld.loggedin", "False");
    
    alert('You have successfully registered the account: ' + un + '. Try logging in now!');
    window.location.href = "index.html";
    
}

function validateLogin(){
    var un = document.getElementById('formLogin').username.value;
    var pw = document.getElementById('formLogin').password.value;
    
    var un2 = localStorage.getItem('vmworld.username');
    var pw2 = localStorage.getItem('vmworld.password');
    
    un3 = un2.replace(/"/g, "");
    pw3 = pw2.replace(/"/g, "");
    
    if ( (un) == (un3)){
        
        if ( (pw) == (pw3)) {
            login();
            localStorage.setItem("vmworld.loggedin", "True")
        }
        
        else{
            alert ('incorrect password. ' + pw + ' /= ' + pw2 + ' try again');
        }
    }
    else {
        alert('incorrect username. ' + un + ' /= ' + un2 + ' try again');
    }
    
}
function amiloggedin(){
    var un2 = localStorage.getItem('vmworld.username');
    var pw2 = localStorage.getItem('vmworld.password');
    
    var loggedIn = localStorage.getItem('vmworld.loggedin');
    
    un3 = un2.replace(/"/g, "");
    pw3 = pw2.replace(/"/g, "");
    
    loggedIn2 = loggedIn.replace(/"/g, "");
    
    if ((loggedIn2) == "True"){
        
        
            login();
    
        
        
    }
    
}


function login() {
    
    var un = document.getElementById('formLogin').username.value;
    var pw = document.getElementById('formLogin').password.value;
    console.log(un);
    console.log(pw);
    localStorage.setItem("vmworld.username", un);
    localStorage.setItem("vmworld.password", pw);
    
    show_members_area();
    
    //localStorage["vmworld.username"] = un;
    //localStorage["vmworld.password"] = pw;
    
    
    }


function supports_html5_storage() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } 
    catch (e) {
        return false;
        alert('Your browser sucks! Use a different browser');
        
    }
    
}
function logout() {
    
    localStorage.setItem("vmworld.loggedin", "False");
    
    $('.showOnLogin').css("display", "none");
    $('.hideOnLogin').css("display", "block");
    
    $('.howTos').css("display","none"); //hide howto in navbar
    $('.nav > .dropdown > .dropdown-toggle').text("Sign In"); //set text to Sign In
    $('#register-link > a').css("display", "Block"); 
    
    alert('You have successfully logged out!');
}

function show_members_area() {
    un = localStorage.getItem("vmworld.username");
    unText = "Logout " + un; 
    $('.howTos').css("display","block"); //show howto in navbar
    $('.nav > .dropdown > .dropdown-toggle').text(unText); //set text to logout
    $('#register-link > a').css("display", "none");
    
    fn = localStorage.getItem("vmworld.firstname");
    ln = localStorage.getItem("vmworld.lastname");
   
    $('.showOnLogin').css("display", "block");
    $('.hideOnLogin').css("display", "none");
    console.log('shown');
}

$( document ).ready(function(){
    
    supports_html5_storage();
    amiloggedin();
    $('#formLogin').submit(function(event){
        validateLogin();
        
       
    });    
    $('#btnLogout').click(function(event){
       logout(); 
    });
});

